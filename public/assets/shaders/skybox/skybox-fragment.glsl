precision mediump float;

varying vec3 textureCoordinates;

uniform vec3 fogColor;

uniform samplerCube daySkyBox;
uniform samplerCube dawnSkyBox;
uniform samplerCube nightSkyBox;

uniform float dayValue;
uniform float dawnValue;
uniform float nightValue;

const float lowerLimit = -120.0;
const float upperLimit = -90.0;

void main(void){
    vec4 dayTextureAmount = textureCube(daySkyBox, textureCoordinates) * dayValue;
    vec4 dawnTextureAmount = textureCube(dawnSkyBox, textureCoordinates) * dawnValue;
    vec4 nightTextureAmount = textureCube(nightSkyBox, textureCoordinates) * nightValue;
    vec4 totalTextureAmount = dayTextureAmount + dawnTextureAmount + nightTextureAmount;

    float factor = (textureCoordinates.y - lowerLimit) / (upperLimit - lowerLimit);
    factor = clamp(factor, 0.5, 0.75);

    gl_FragColor = mix(vec4(fogColor, 1.0), totalTextureAmount, factor);
}