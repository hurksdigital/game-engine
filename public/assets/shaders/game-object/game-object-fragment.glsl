precision mediump float;

varying vec2 passTextureCoordinates;
varying vec3 surfaceNormal;
varying vec3 toLightVector[MAX_LIGHTS];
varying vec3 toCameraVector;
varying float visibility;
varying float clipFragment;

uniform sampler2D textureSampler;
uniform vec3 lightColor[MAX_LIGHTS];
uniform float shineDamper;
uniform float reflectivity;
uniform vec3 skyColor;
uniform vec3 attenuation[MAX_LIGHTS];
uniform float dayLightValue;

void main () {
    if (clipFragment == 1.0) {
        discard;
    }

    vec3 unitNormal = normalize(surfaceNormal);
    vec3 unitToCameraVector = normalize(toCameraVector);

    vec3 totalDiffuse = vec3(0.0);
    vec3 totalSpecular = vec3(0.0);

    for (int i = 0; i < MAX_LIGHTS; i ++) {
        float distance = length(toLightVector[i]);
        float attenuationFactor = attenuation[i].x + (attenuation[i].y * distance) + (attenuation[i].z * distance * distance);
        vec3 unitLightVector = normalize(toLightVector[i]);
        float nDot1 = dot(unitNormal, unitLightVector);
        float brightness = max(nDot1, 0.0);
        vec3 lightDirection = -unitLightVector;
        vec3 reflectedLightDirection = reflect(lightDirection, unitNormal);

        float specularFactor = dot(reflectedLightDirection, unitToCameraVector);
        specularFactor = max(specularFactor, 0.0);
        float dampedFactor = pow(specularFactor, shineDamper);

        totalDiffuse = totalDiffuse + (brightness * lightColor[i])/attenuationFactor;
        totalSpecular = totalSpecular + (dampedFactor * reflectivity * lightColor[i])/attenuationFactor;
    }
    totalDiffuse = max(totalDiffuse, dayLightValue * 0.5);

    vec4 textureColor = texture2D(textureSampler, passTextureCoordinates, -0.4);
    if (textureColor.a < 0.5) {
        discard;
    }

    gl_FragColor = vec4(totalDiffuse, 1.0) * textureColor + vec4(totalSpecular, 1.0);
    gl_FragColor = mix(vec4(skyColor, 1.0), gl_FragColor, visibility);
}