precision mediump float;

varying vec4 clipSpaceCoordinates;
varying vec2 textureCoordinates;
varying vec3 toCameraVector;
varying vec3 fromLightVector;

uniform sampler2D reflectionTexture;
uniform sampler2D refractionTexture;
uniform sampler2D dudvTexture;
uniform sampler2D normalMapTexture;
uniform sampler2D depthMapTexture;
uniform float moveFactor;
uniform vec3 sunColor;
uniform float farPlane;
uniform float nearPlane;

const float waveStrength = 0.02;
const float shineDamper = 30.0;
const float reflectivity = 0.6;

void main(void) {

    vec2 normalizedDeviceCoordinates = (clipSpaceCoordinates.xy/clipSpaceCoordinates.w) / 2.0 + 0.5;
    vec2 refractTextureCoordinates = vec2(normalizedDeviceCoordinates.xy);
    vec2 reflectTextureCoordinates = vec2(normalizedDeviceCoordinates.x, -normalizedDeviceCoordinates.y);

    float depth = texture2D(depthMapTexture, refractTextureCoordinates).r;
    float floorDistance = 2.0 * nearPlane * farPlane / (farPlane + nearPlane - (2.0 * depth - 1.0) * (farPlane - nearPlane));

    depth = gl_FragCoord.z;
    float waterDistance = 2.0 * nearPlane * farPlane / (farPlane + nearPlane - (2.0 * depth - 1.0) * (farPlane - nearPlane));
    float waterDepth = floorDistance - waterDistance;

    vec2 distortedTextureCoordinates = texture2D(dudvTexture, vec2(textureCoordinates.x + moveFactor, textureCoordinates.y)).rg * 0.1;
    distortedTextureCoordinates = textureCoordinates + vec2(distortedTextureCoordinates.x, distortedTextureCoordinates.y + moveFactor);
    vec2 totalDistortion = (texture2D(dudvTexture, distortedTextureCoordinates).rg * 2.0 - 1.0) * waveStrength * clamp(waterDepth/20.0, 0.0, 1.0);

    refractTextureCoordinates += totalDistortion;
    refractTextureCoordinates = clamp(refractTextureCoordinates, 0.001, 0.999);

    reflectTextureCoordinates += totalDistortion;
    reflectTextureCoordinates.x = clamp(reflectTextureCoordinates.x, 0.001, 0.999);
    reflectTextureCoordinates.y = clamp(reflectTextureCoordinates.y, -0.999, -0.001);

    vec4 reflectColor = texture2D(reflectionTexture, reflectTextureCoordinates);
    vec4 refractColor = texture2D(refractionTexture, refractTextureCoordinates);

    vec4 normalMapColor = texture2D(normalMapTexture, distortedTextureCoordinates);
    vec3 normal = vec3(normalMapColor.r * 2.0 - 1.0, normalMapColor.b * 3.0, normalMapColor.g * 2.0 - 1.0);
    normal = normalize(normal);

    vec3 viewVector = normalize(toCameraVector);
    float refractiveFactor = dot(viewVector, normal);
    refractiveFactor = pow(refractiveFactor, 0.5);
    refractiveFactor = clamp(refractiveFactor, 0.0, 1.0);

    vec3 reflectedLight = reflect(normalize(fromLightVector), normal);
    float specular = max(dot(reflectedLight, viewVector), 0.0);
    specular = pow(specular, shineDamper);
    vec3 specularHighlights = sunColor * specular * reflectivity * clamp(waterDepth / 5.0, 0.0, 1.0);

	gl_FragColor = mix(reflectColor, refractColor, refractiveFactor);
    gl_FragColor = mix(gl_FragColor, vec4(0.0, 0.3, 0.5, 1.0), 0.4) + vec4(specularHighlights, 0.0);
    gl_FragColor.a = clamp(waterDepth / 5.0, 0.0, 1.0);
    // gl_FragColor = vec4(moveFactor, 0.0, 0.0, 1.0);
    gl_FragColor = texture2D(depthMapTexture, distortedTextureCoordinates);
}