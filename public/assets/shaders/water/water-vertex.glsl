attribute vec2 position;

varying vec4 clipSpaceCoordinates;
varying vec2 textureCoordinates;
varying vec3 toCameraVector;
varying vec3 fromLightVector;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 transformationMatrix;
uniform float waterSize;
uniform vec3 cameraPosition;
uniform vec3 sunPosition;

const float tiling = 0.1;

void main(void) {

    vec4 worldPosition = transformationMatrix * vec4(position.x, 0, position.y, 1.0);
    clipSpaceCoordinates = projectionMatrix * viewMatrix * worldPosition;
	gl_Position = clipSpaceCoordinates;
    textureCoordinates = vec2(position.x/2.0 + 0.5, position.y/2.0 + 0.5) * tiling * waterSize;
    toCameraVector = cameraPosition - worldPosition.xyz;
    fromLightVector = worldPosition.xyz - sunPosition;

}