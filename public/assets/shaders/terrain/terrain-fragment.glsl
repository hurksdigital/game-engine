precision mediump float;

varying float passHeight;
varying vec2 passTextureCoordinates;
varying vec3 surfaceNormal;
varying vec3 toLightVector[MAX_LIGHTS];
varying vec3 toCameraVector;
varying float visibility;
varying float clipFragment;

uniform sampler2D backgroundTexture;
uniform sampler2D redTexture;
uniform sampler2D greenTexture;
uniform sampler2D blueTexture;
uniform sampler2D blendMapTexture;

uniform vec3 lightColor[MAX_LIGHTS];
uniform vec3 attenuation[MAX_LIGHTS];
uniform float shineDamper;
uniform float reflectivity;
uniform vec3 skyColor;

uniform float dayLightValue;

void main () {
    if (clipFragment == 1.0) {
        discard;
    }

    vec4 blendMapColor = texture2D(blendMapTexture, passTextureCoordinates, -0.4);

    float backTextureAmount = 1.0 - (blendMapColor.r + blendMapColor.g + blendMapColor.b);
    vec2 tiledCoords = passTextureCoordinates * 80.0;
    vec4 backgroundTextureColor = texture2D(backgroundTexture, tiledCoords, -0.4) * backTextureAmount;
    vec4 redTextureColor = texture2D(redTexture, tiledCoords, -0.4) * blendMapColor.r;
    vec4 greenTextureColor = texture2D(greenTexture, tiledCoords, -0.4) * blendMapColor.g;
    vec4 blueTextureColor = texture2D(blueTexture, tiledCoords, -0.4) * blendMapColor.b;

    vec4 totalColor = mix(
        vec4(skyColor, 1.0),
        backgroundTextureColor + redTextureColor + greenTextureColor + blueTextureColor,
        1.0
    );

    vec3 totalDiffuse = vec3(0.0);
    vec3 totalSpecular = vec3(0.0);
    for (int i = 0; i < MAX_LIGHTS; i ++) {
        float distance = length(toLightVector[i]);
        float attenuationFactor = attenuation[i].x + (attenuation[i].y * distance) + (attenuation[i].z * distance * distance);
        vec3 unitNormal = normalize(surfaceNormal);
        vec3 unitLightVector = normalize(toLightVector[i]);
        float nDot1 = dot(unitNormal, unitLightVector);
        float brightness = max(nDot1, 0.0);
        vec3 unitToCameraVector = normalize(toCameraVector);
        vec3 lightDirection = -unitLightVector;
        vec3 reflectedLightDirection = reflect(lightDirection, unitNormal);

        float specularFactor = dot(reflectedLightDirection, unitToCameraVector);
        specularFactor = max(specularFactor, 0.0);
        float dampedFactor = pow(specularFactor, shineDamper);

        totalDiffuse = totalDiffuse + (brightness * lightColor[i])/attenuationFactor;
        totalSpecular = totalSpecular + (dampedFactor * reflectivity * lightColor[i])/attenuationFactor;
    }
    totalDiffuse = max(totalDiffuse, dayLightValue * 0.5);

    gl_FragColor = vec4(totalDiffuse, 1.0) * totalColor + vec4(totalSpecular, 1.0);
    gl_FragColor = mix(vec4(skyColor, 1.0), gl_FragColor, visibility);
}