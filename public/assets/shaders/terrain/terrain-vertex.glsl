precision mediump float;

attribute vec3 position;
attribute vec2 textureCoordinates;
attribute vec3 normal;

varying vec2 passTextureCoordinates;
varying vec3 surfaceNormal;
varying vec3 toLightVector[MAX_LIGHTS];
varying vec3 toCameraVector;
varying float visibility;
varying float clipFragment;
varying float passHeight;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 inverseViewMatrix;
uniform vec3 lightPosition[MAX_LIGHTS];
uniform float fogAmount;
uniform vec4 plane;

const float density = 0.001;
const float gradient = 1.5;

void main (void) {

    vec4 worldPosition = transformationMatrix * vec4(position, 1.0);
    passHeight = worldPosition.y;

    clipFragment = 0.0;
    if (plane.y < 0.0) {
        if (worldPosition.y > plane.w) {
            clipFragment = 1.0;
        }
    }
    else if (plane.y > 0.0) {
        if (worldPosition.y < plane.w) {
            clipFragment = 1.0;
        }
    }

    if (clipFragment == 0.0) {
        vec4 positionRelativeToCamera = viewMatrix * worldPosition;

        gl_Position = projectionMatrix * positionRelativeToCamera;
        passTextureCoordinates = textureCoordinates;

        surfaceNormal = (transformationMatrix * vec4(normal, 0.0)).xyz;
        for (int i = 0; i < MAX_LIGHTS; i ++) {
            toLightVector[i] = lightPosition[i] - worldPosition.xyz;
        }
        toCameraVector = (inverseViewMatrix * vec4(0.0, 0.0, 0.0, 1.0)).xyz - worldPosition.xyz;

        float distance = length(positionRelativeToCamera.xyz);
        visibility = clamp(exp(-pow((distance * density * fogAmount), gradient)), 0.0, 1.0);
    }
}