/**
 * A 3d model stored in memory.
 * 
 * @author Stan Hurks
 */
export default interface GameObjectModelComponent {

    /**
     * The amount of indices
     */
    indicesCount: number

    /**
     * The array buffer
     */
    arrayBuffer?: WebGLBuffer

    /**
     * The element array buffer
     */
    elementArrayBuffer?: WebGLBuffer
}