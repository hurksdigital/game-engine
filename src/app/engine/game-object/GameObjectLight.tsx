import Vector3 from '../math/Vector3'

/**
 * A light from a game object.
 * 
 * @author Stan Hurks
 */
export default interface GameObjectLight {

    /**
     * The position of the light relative to the GameObject
     */
    position: Vector3

    /**
     * The color of the light (r, g, b)
     */
    color: Vector3

    /**
     * The attenuation of the light
     */
    attenuation?: Vector3
}