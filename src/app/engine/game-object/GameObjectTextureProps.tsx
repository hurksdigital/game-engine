/**
 * The base for a game object texture.
 * 
 * @author Stan Hurks
 */
export default interface GameObjectTextureBase {

    /**
     * How close the camera needs to be to see reflected light
     */
    shineDamper?: number

    /**
     * The factor of reflectivity of lights
     */
    reflectivity?: number

    /**
     * Whether or not the texture is transparent
     */
    isTransparent?: boolean

    /**
     * Whether or not to use fake lighting.
     * 
     * This is used when the object is a plane and
     * by setting this to true the normal point upwards.
     */
    useFakeLighting?: boolean

    /**
     * The options for when the texture is a sprite
     */
    sprite?: {
        
        /**
         * The amount of rows the texture has if it is a sprite.
         * Sprites need to have the same amount of rows as columns.
         * So a sprite with 3 rows will have 9 sprite indices.
         */
        rows: number
    
        /**
         * The current sprite index
         */
        currentIndex: number
    }
}