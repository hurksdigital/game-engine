import GameObjectTextureBase from './GameObjectTextureProps'

/**
 * The texture that belongs to a Game Object.
 * 
 * @author Stan Hurks
 */
export default interface GameObjectTexture extends GameObjectTextureBase {

    /**
     * The texture in WebGL
     */
    texture: WebGLTexture
}