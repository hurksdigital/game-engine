import GameObjectLight from './GameObjectLight'
import Vector3 from '../math/Vector3'

/**
 * The sun.
 * 
 * @author Stan Hurks
 */
export default class Sun implements GameObjectLight {

    constructor(public position: Vector3, public color: Vector3, public attenuation?: Vector3) {
        this.position = position
        this.color = color
        this.attenuation = attenuation
    }
}