import GameObjectModelComponent from './GameObjectModelComponent'
import GameObjectTexture from './GameObjectTexture'

/**
 * A game object model.
 * 
 * @author Stan Hurks
 */
export default interface GameObjectModel {

    /**
     * The name of the model
     */
    name: string

    /**
     * The components of the model
     */
    components: Array<GameObjectModelComponent & {
        
        /**
         * The component name (or null if)
         */
        name?: string
    }>

    /**
     * The texture
     */
    texture: GameObjectTexture
}