import Vector3 from '../../math/Vector3'

/**
 * A single water tile.
 * 
 * @author Stan Hurks
 */
export default class WaterTile {

    /**
     * The height of the water
     */
    public static readonly WATER_HEIGHT = 0

    /**
     * The position of the water
     */
    public position: Vector3

    /**
     * The scale
     */
    public scale: number

    constructor(position: Vector3, scale: number) {
        this.position = position
        this.scale = scale
    }
}