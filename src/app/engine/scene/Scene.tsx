import SceneTerrain from './SceneTerrain'
import SceneWaterTile from './SceneWaterTile'
import SceneGameObject from './SceneGameObject'

/**
 * A scene in the game.
 * 
 * @author Stan Hurks
 */
export default interface Scene {
    
    /**
     * The name of the scene
     */
    name: string

    /**
     * The terrains in the scene
     */
    terrains: SceneTerrain[]

    /**
     * The water options in the scene
     */
    water: {
        /**
         * The height of the water in the scene
         */
        height: number

        /**
         * The water tiles in the scene
         */
        tiles: SceneWaterTile[]
    }

    /**
     * The game objects in the scene
     */
    gameObjects: SceneGameObject[]
}