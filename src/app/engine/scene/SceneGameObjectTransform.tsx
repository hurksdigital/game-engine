import Vector3 from '../math/Vector3'

/**
 * The transform in a scene game object.
 * 
 * @author Stan Hurks
 */
export default interface SceneGameObjectTransform {

    /**
     * The position
     */
    position: Vector3

    /**
     * The rotation
     */
    rotation?: Vector3

    /**
     * The scale
     */
    scale?: Vector3
}