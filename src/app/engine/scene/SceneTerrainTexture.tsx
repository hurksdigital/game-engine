import Vector4 from '../math/Vector4'

/**
 * A texture in the scene terrain.
 * 
 * @author Stan Hurks
 */
export default interface SceneTerrainTexture {

    /**
     * The color of the texture
     */
    color: number[]

    /**
     * The location of the texture in the asset folder
     */
    location?: string
}