import Vector4 from '../math/Vector4'
import GameObjectTextureBase from '../game-object/GameObjectTextureProps'

/**
 * The definition for a model in the scene.
 * 
 * @author Stan Hurks
 */
export default interface SceneModelDefinition {

    /**
     * The texture definition
     */
    texture: GameObjectTextureBase & {

        /**
         * The color of the texture
         */
        color: number[]

        /**
         * The texture location relative to the assets folder
         * 
         * E.g.: if the texture path is /assets/textures/1.png this value should be `textures/1`.
         */
        location?: string

        /**
         * Whether or not to repeat the image for the texture on the object.
         */
        repeatImage?: boolean
    }
}