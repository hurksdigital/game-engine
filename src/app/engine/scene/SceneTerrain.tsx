import SceneTerrainTexture from './SceneTerrainTexture'

/**
 * A terrain in the scene.
 * 
 * @author Stan Hurks
 */
export default interface SceneTerrain {

    /**
     * The name of the terrain
     */
    name: string

    /**
     * The size of the terrain in coordinates
     */
    size: number

    /**
     * The position of the terrain in world space
     */
    position: {
        x: number

        z: number
    }

    /**
     * The textures of the terrain
     */
    textures: {
        black: SceneTerrainTexture

        red: SceneTerrainTexture

        blue: SceneTerrainTexture

        green: SceneTerrainTexture
    }
}