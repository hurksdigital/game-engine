import Vector3 from '../math/Vector3'

/**
 * A water tile in the scene
 * 
 * @author Stan Hurks
 */
export default interface SceneWaterTile {

    /**
     * The position of the water tile
     */
    position: Vector3

    /**
     * The scale
     */
    scale: number
}