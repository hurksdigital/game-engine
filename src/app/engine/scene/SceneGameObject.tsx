import GameObjectLight from '../game-object/GameObjectLight'
import SceneGameObjectTransform from './SceneGameObjectTransform'

/**
 * A game object in the scene.
 * 
 * @author Stan Hurks
 */
export default interface SceneGameObject {

    /**
     * The ID of the game object
     */
    id?: string

    /**
     * The model name
     */
    modelName: string

    /**
     * The transform of the game object
     */
    transform: SceneGameObjectTransform

    /**
     * The light of the game object
     */
    light?: GameObjectLight
}