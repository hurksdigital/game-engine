/**
 * A game loop.
 * 
 * @author Stan Hurks
 */
export default interface GameLoop {

    /**
     * Initialize the game loop
     */
    initialize: () => Promise<void>

    /**
     * Perform the game loop
     */
    loop: () => void

    /**
     * Clean up the game loop
     */
    cleanUp: () => void
}