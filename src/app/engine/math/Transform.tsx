import Vector3 from './Vector3'

/**
 * All transform properties for a game object.
 * 
 * @author Stan Hurks
 */
export default interface Transform {

    /**
     * The position
     */
    position: Vector3

    /**
     * The rotation
     */
    rotation: Vector3

    /**
     * The scale
     */
    scale: Vector3
}