import moment from 'moment'

/**
 * The translations for locale: 'nl_NL'
 * 
 * @author Stan Hurks
 */
const TranslationsNLNL = {
    components: {
        loader: {
            allRightsReserved: 'Alle rechten voorbehouden.'
        },
        progressSpinner: {
            loading: 'Laden...'
        }
    },

    date: {
        /**
         * Generates a human readable date based on the language
         */
        humanReadable: (timestamp: number): string => {
            const timestampNow = new Date().getTime()
            const delta = timestampNow - timestamp
            const nowDate = moment(timestampNow).format('DD-MM-YYYY')
            const timestampDate = moment(timestamp).format('DD-MM-YYYY')

            if (delta > 0) {
                // past 5 minutes
                if (delta <= 1000 * 60 * 5) {
                    return 'Zojuist'
                }

                // Past hour
                else if (delta <= 1000 * 60 * 60) {
                    return `${Math.round(delta / 1000 / 60)} minuten`
                }

                // Same day
                else if (nowDate === timestampDate) {
                    return `${Math.round(delta / 1000 / 60 / 60)} uur`
                }

                // Yesterday
                else if (delta < 2 * 24 * 3600 * 1000) {
                    return 'Gisteren'
                }

                // 2 days ago
                else if (delta < 3 * 24 * 3600 * 1000) {
                    return 'Eergisteren'
                }

                // days
                else if (delta < 7 * 24 * 3600 * 1000) {
                    return `${Math.round(delta / 1000 / 60 / 60 / 24)} dagen`
                }

                // 1 week
                else if (delta < 2 * 7 * 24 * 3600 * 1000) {
                    return '1 Week'
                }

                // Multiple weeks
                else {
                    return `${Math.round(delta / 1000 / 60 / 60 / 24 / 7)} weken`
                }
            } else {
                return 'Zojuist'
            }
        },

        month: [
            'Januari',
            'Februari',
            'Maart',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Augustus',
            'September',
            'Oktober',
            'November',
            'December'
        ]
    },

    document: {
        lang: 'nl',
        meta: {
            description: 'WebGL game engine door Hurks Digital B.V.'
        },
        title: {
            main: 'Game engine',
        }
    }
}
export default TranslationsNLNL